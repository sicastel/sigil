open Cmdliner
open Core
open LinearA
open Lwt
open Websocket
open Websocket_lwt_unix

let db = ref (None : Database.t option)

let get_db () = match !db with Some db -> db | _ -> assert false

let set_db new_db = db := Some new_db

let frame_of_string msg = Frame.(create ~opcode:Opcode.Text ~content:msg ())

type client_state = {auth: bool}

let password = "password"

let handle_question (type a) _corpus _dir client :
    a Protocol.request -> client_state * a Protocol.reply = function
  | {question = Protocol.Hello; _} -> (client, Ok (get_db ()))
  (* | {question = EditDocumentComment (name, note); auth} -> let html =
     Prelude.markdown note in if auth = Some password then let () = set_db
     (Database.set_document_note (get_db ()) corpus dir name (note, html)) in
     (client, Ok html) else (client, Error "Edits only allowed for
     authentified users.") | {question = EditOccurrenceComment (name, note,
     n); auth} -> let html = Prelude.markdown note in if auth = Some password
     then let () = set_db (Database.set_occurrence_note (get_db ()) corpus
     dir name n (note, html)) in (client, Ok html) else (client, Error "Edits
     only allowed for authentified users.")*)
  | {question = Auth pw; _} ->
      if pw = "password" then ({auth = true}, Ok ())
      else (client, Error "Invalid password")
  | _ -> failwith "Unknown message."

let handle_message corpus dir client client_state msg =
  match Protocol.request_of_string msg with
  | Ok ({Protocol.question = Q question; _} as r) ->
      let client_state, answer =
        handle_question corpus dir client_state {r with question}
      in
      let string = Protocol.string_of_reply question answer in
      Printf.eprintf "Message: %s" string ;
      Connected_client.send client (frame_of_string string)
      >>= fun _ -> return client_state
  | (exception Failure s) | Error s ->
      Printf.eprintf "Error when handling message: %s\n" s ;
      return client_state

let handle_client corpus dir client =
  let rec inner state =
    Connected_client.recv client
    >>= fun fr ->
    match fr.opcode with
    | Frame.Opcode.Ping ->
        Connected_client.send client
          Frame.(create ~opcode:Opcode.Pong ~content:fr.content ())
        >>= fun _ -> inner state
    | Frame.Opcode.Close ->
        (* Immediately echo and pass this last message to the user *)
        if String.length fr.content >= 2 then
          let content = String.sub fr.content 0 2 in
          Connected_client.send client
            Frame.(create ~opcode:Opcode.Close ~content ())
        else Connected_client.send client @@ Frame.close 1000
    | Frame.Opcode.Pong -> inner state
    | Frame.Opcode.Text | Frame.Opcode.Binary ->
        Lwt.catch
          (fun () -> handle_message corpus dir client state fr.content)
          (fun _ -> return state)
        >>= inner
    | _ -> Connected_client.send client Frame.(close 1002)
  in
  inner {auth = false}

let setup_server corpus db_path host port path =
  let _ =
    set_db (Prelude.report_error @@ Database.load_from_directory db_path)
  in
  let uri = Uri.make ~scheme:"http" ~port ~host ~path () in
  let handle_client client =
    Lwt.catch
      (fun () -> handle_client corpus db_path client)
      (fun exn ->
        Printf.eprintf "Exception: %s\n%!" (Printexc.to_string exn) ;
        return () )
  in
  Lwt_main.run
    ( Resolver_lwt.resolve_uri ~uri Resolver_lwt_unix.system
    >>= fun endp ->
    let open Conduit_lwt_unix in
    endp_to_server ~ctx:default_ctx endp
    >>= fun server ->
    establish_server ~ctx:default_ctx ~mode:server handle_client )

(* CLI options *)

let db_path =
  let doc = "Directory where the database is located." in
  Arg.(
    required & opt (some string) None & info ["db-path"] ~docv:"DB-PATH" ~doc)

let port =
  let doc = "Port to listen on." in
  Arg.(required & opt (some int) None & info ["p"; "port"] ~docv:"PORT" ~doc)

let host =
  let doc = "Host to bind." in
  Arg.(required & opt (some string) None & info ["host"] ~docv:"HOST" ~doc)

let path =
  let doc = "Path to bind in the scheme." in
  Arg.(required & opt (some string) None & info ["path"] ~docv:"PATH" ~doc)

let corpus =
  let doc = "Path to the corpus." in
  Arg.(
    required & opt (some string) None & info ["corpus"] ~docv:"corpus" ~doc)

let info_sigla_server =
  Cmdliner.Term.info ~doc:"Starts the SigLA websocket server." "siglaServer"

let _ =
  Cmdliner.Term.(
    eval
      ( const setup_server $ corpus $ db_path $ host $ port $ path
      , info_sigla_server ))
