(** Defining the Linear A script. *)
open Core

type sign =
  { prefix: string
  ; number: int
  ; phonetic: PhoneticValue.t option
  ; rep: string option
  ; latin: string option
  ; variants: (string * string) list [@default []]
  ; vessel: bool
  ; comment: (string * string) option [@default None]
  ; composition: string option }

and instance = {sign: sign; variant: string option [@default None]}

and reading =
  | Unreadable
  | Unclassified
  | Readable of {instance: instance; confidence: bool}

and composition = reading Composition.t [@@deriving yojson]

module DB : sig
  val load : string -> unit

  val load_from_string : string -> unit

  val to_string : unit -> string
end

module Sign : sig
  type t = sign [@@deriving yojson]

  val conv : t Conv.t

  val ty : t Ty.t

  val instances : t -> instance list

  val list : unit -> t list

  val show : t -> string

  val show_full : t -> string

  val show_syllabogram : t -> string

  val default_role : t -> Role.t
end

module Instance : sig
  type t = instance [@@deriving yojson]

  val parse : t Parsing.t

  val show : ?f:(Sign.t -> string) -> t -> string

  val conv : t Conv.t

  val ty : t Ty.t

  val list : unit -> t list

  val matches : t -> t -> bool
end

module Reading : sig
  type t = reading [@@deriving yojson]

  val show : ?f:(Sign.t -> string) -> t -> string

  val parse : t Parsing.t

  val conv : t Conv.t

  val ty : t Ty.t

  val matches : t -> t -> bool

  val sign : t -> Sign.t option
end

module CompositionPattern : sig
  type t = Reading.t option * bool

  val parse : t Parsing.t

  val conv : t Conv.t

  val ty : t Ty.t

  val matches : t -> reading -> bool
end

val composition : Sign.t -> composition option
