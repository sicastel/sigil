open Core

type t = Here | Not_here | Unsure [@@deriving yojson]

type bounds = t * t [@@deriving yojson]

val show_before : t -> string

val show_after : t -> string

val show : bounds -> string -> string

val parse : 'a Parsing.t -> ('a * bounds) Parsing.t
