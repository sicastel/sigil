open Core

type 'answer question =
  | Hello : Database.t question
  | EditDocumentComment : string * string -> string question
  | EditOccurrenceComment : string * string * int -> string question
  | EditSignComment : string * string -> string question
  | Auth : string -> unit question

type _question = Q : 'answer question -> _question

type 'question _request = {question: 'question; auth: string option}
[@@deriving yojson]

type 'answer request = 'answer question _request

type 'a reply = Ok of 'a | Error of string [@@deriving yojson]

val request_of_string : string -> _question _request Prelude.optional

val string_of_request : 'a request -> string

val reply_of_string : string -> 'a question -> 'a reply Prelude.optional

val string_of_reply : 'a question -> 'a reply -> string
