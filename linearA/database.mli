open Core

type t [@@deriving yojson]

val load_from_directory : string -> t Prelude.optional

val save_to_directory : string -> t -> unit

val find_document : t -> string -> Document.t

val documents : t -> Document.t list

(*val set_document_note : t -> string -> string -> string -> string * string
  -> t

  val set_occurrence_note : t -> string -> string -> string -> int -> string
  * string -> t *)
val create : docs:Document.t list -> t

val of_string : string -> t

val locations : t -> string list

val periods : t -> string list

val scribes : t -> string list

val motifs : t -> string list

val find_spots : t -> string list

val write_file : t -> string -> unit

val attestations_such_that :
  t -> (Document.Attestation.t -> bool) -> Document.Attestation.t list

val sequences_such_that :
  t -> (Document.Sequence.t -> bool) -> Document.Sequence.t list

val representative_image_sign : Script.Sign.t -> string option

val representative_image_inst : Script.Instance.t -> string option

val types : t -> string list

val check_representative : t -> unit

(** Enumerating types *)

val register_enumerate : (t -> 'a list) -> 'a Ty.t -> unit

val is_enumerable : 'a Ty.t -> bool

val enumerate : t -> 'a Ty.t -> 'a list
