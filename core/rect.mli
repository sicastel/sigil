type t = {x: int; y: int; w: int; h: int} [@@deriving yojson]

val bounds : t list -> t list
