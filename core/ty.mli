open Prelude

type 'a custom = ..

type 'a t =
  | Arrow : 'a t * 'b t -> ('a -> 'b) t
  | List : 'a t -> 'a list t
  | Pair : 'a t * 'b t -> ('a * 'b) t
  | Custom : (string * 'a custom) -> 'a t

val show : 'a t -> string

val ( *** ) : 'a t -> 'b t -> ('a * 'b) t

val ( --> ) : 'a t -> 'b t -> ('a -> 'b) t

val list : 'a t -> 'a list t

val int : int t

val string : string t

val bool : bool t

val float : float t

val unit : unit t

val test : 'a t -> 'b t -> ('a, 'b) Equality.t optional

val equal : 'a t -> 'b t -> bool

val ( = ) : 'a t -> 'b t -> bool

val cast : 'a t -> 'b t -> 'a -> 'b optional

module Custom : sig
  type 'a data =
    { conv: 'a Conv.t option
    ; name: string
    ; friendly: string
    ; constructor: 'a custom
    ; cast: 'b. 'b custom -> ('a, 'b) Equality.t option
    ; compare: 'a -> 'a -> int }

  val add :
       ?conv:'a Conv.t
    -> ?compare:('a -> 'a -> int)
    -> friendly:string
    -> string
    -> 'a t
end

module Untyped : sig
  type _t = T : 'a t -> _t

  type t = _t

  val conv : t Conv.t
end

module Methods : sig
  val conv : 'a t -> 'a Conv.t

  val compare : 'a t -> 'a -> 'a -> int

  val show : 'a t -> 'a -> string

  val parse : 'a t -> 'a Parsing.t

  val to_yojson : 'a t -> 'a -> Yojson.Safe.t

  val of_yojson : 'a t -> Yojson.Safe.t -> 'a optional
end

module Object : sig
  type obj = O : 'a t * 'a -> obj

  val conv : obj Conv.t

  val unpack : 'a t -> obj -> 'a optional

  type Markup.t += Link of obj * Markup.t
end

module Matcher (S : sig
  type 'a t
end) : sig
  type clause = C : 'a t * 'a S.t -> clause

  type list_clause = {list: 'a. 'a S.t -> 'a list S.t}

  type pair_clause = {pair: 'a 'b. 'a S.t -> 'b S.t -> ('a * 'b) S.t}

  val on : 'a t -> return:'a S.t -> clause

  val run_clauses : 'a t -> clause list -> 'a S.t optional

  val run :
       ?list:list_clause
    -> ?pair:pair_clause
    -> clause list
    -> 'a t
    -> 'a S.t optional

  val run_def :
       ?list:list_clause
    -> ?pair:pair_clause
    -> default:'a S.t
    -> clause list
    -> 'a t
    -> 'a S.t

  type create_function = {f: 'a. 'a t -> 'a S.t -> unit}

  type dictionary = clause list ref * create_function

  val create : unit -> dictionary

  val add : dictionary -> 'a t -> 'a S.t -> unit

  val lookup :
       ?list:list_clause
    -> ?pair:pair_clause
    -> default:'a S.t
    -> dictionary
    -> 'a t
    -> 'a S.t

  val lookup_opt :
       ?list:list_clause
    -> ?pair:pair_clause
    -> dictionary
    -> 'a t
    -> 'a S.t optional
end
