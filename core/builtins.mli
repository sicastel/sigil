type ('param, 'inp, 'out) t =
  { name: string
  ; friendly: string
  ; eval: 'param -> 'inp -> 'out
  ; description: string option
  ; param: 'param Ty.t
  ; ret: 'out Ty.t
  ; inp: 'inp Ty.t }

type 'inp _t = B : ('param, 'inp, 'out) t -> 'inp _t

type untyped = U : ('param, 'inp, 'out) t -> untyped

val eval : ('param, 'inp, 'out) t -> 'param -> 'inp -> 'out

val register :
     ?description:string
  -> friendly:string
  -> eval:('a -> 'b -> 'c)
  -> param:'a Ty.t
  -> inp:'b Ty.t
  -> ret:'c Ty.t
  -> string
  -> unit

val register_ :
     ?description:string
  -> friendly:string
  -> eval:('a -> 'b -> 'c)
  -> param:'a Ty.t
  -> inp:'b Ty.t
  -> ret:'c Ty.t
  -> string
  -> ('a, 'b, 'c) t

val typ_of : ('param, 'inp, 'out) t -> ('param -> 'inp -> 'out) Ty.t

val find_by_name : string -> 'inp Ty.t -> 'inp _t

val find_by_name_untyped : string -> untyped

val of_typ : 'a Ty.t -> 'a _t list

val belongs : 'a Ty.t -> ('a list, 'a, bool) t

type 'inp classification =
  | Pred : ('param, 'inp, bool) t -> 'inp classification
  | Prop : (unit, 'inp, 'out) t -> 'inp classification

val classify : 'inp _t -> 'inp classification

val is_property : 'inp _t -> bool
