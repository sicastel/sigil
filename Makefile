all:
	dune build

release:
	dune build --profile release
server:
	./_build/default/server/sigilServer.exe --corpus ../sigla/corpus/ --db-path ../sigla/db/ --port 4343 --host localhost --path '/'

view:
	spawn sh -c 'cd db/; python2 -m SimpleHTTPServer'
	firefox 'http://localhost:8000'

.PHONY: server
