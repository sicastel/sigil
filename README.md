% Sigil: A web interface to consult ancient documents

Sigil is a software to manipulate a display a database of ancient
document, mainly targeted at LinearA through the
[SigLA](https://sigla.phis.me) database.

# What does sigil do
Sigil transforms an input corpus of ancient inscriptions into a
web-browsable database.

## The input of Sigil: the corpus
We call the input of Sigil: the corpus. It is supposed to be a
directory containing:

- A `signs.json` file documenting the signs available on the
  inscriptions.
  
- For each inscription (for now) two files: a file `inscription.kra`
  which contains the image of the tablet in layers, and a file
  `inscription.json` containing the metadata.
  
Each sign of the inscription must be on a separate layer with a label
describing the sign.

## The output: A browsable database
The output produced by sigil is a browsable database. In particular:

- A page per document with an interactive view of its signs in `document/Tablet Name/`
- A page per location / document type / ... in for instance `location/Location`
- Search pages, indices, ...
- For each file in .static.html in the database directory, it also
  creates a full .html file.
  

This is the code for the web interface of SigLA. We describe the
organisation of the code as well as the format of the database.

## Invocation
Simply:

    sigilImport corpus/ -o database/

You can then upload the contents of `database/` on your hosting
website. You will need to include the javascript code in `database`.

# Organisation of the code

- `core`: basic code that contains in particular most of the search
  abstract mechanism (groups, λ-terms, serialisation, ...)
- `linearA`: all LinearA-specific code
- `client`: OCaml code meant to be compiled to Javascript
- `generation`: Generation of HTML page
- `importer`: main entry point of sigil: code that imports data from
  the corpus and controls generation of code.
