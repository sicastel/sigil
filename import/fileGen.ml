module Model = struct
  include Tyxml

  module Dynamic = struct
    let show s =
      Printf.sprintf "document.getElementById(%S).style.display=%S" s "block"

    let hide s =
      Printf.sprintf "document.getElementById(%S).style.display=%S" s "none"
  end
end

open Generation
module Basic = Basic.Make (Model)
module Sign = Sign.Make (Model)
module Grouping = Grouping.Make (Model)
module Pages = Pages.Make (Model)
module H = Home.Make (Model)
module HDoc = HDocument.Make (Model)

let compute_root file =
  let s = ref "" in
  let () = String.iter (fun c -> if c = '/' then s := "../" ^ !s) file in
  !s

let generate dir (output, x) =
  let output =
    if output.[String.length output - 1] = '/' then output ^ "index.html"
    else output
  in
  let root = compute_root output in
  let html = Url.with_root root (fun _ -> Basic.render (x ())) in
  let _ =
    Sys.command
    @@ Printf.sprintf "mkdir -p %s"
         Filename.(concat dir @@ quote (dirname output))
  in
  let fd = open_out (Filename.concat dir output) in
  Format.fprintf
    (Format.formatter_of_out_channel fd)
    "%a"
    (Model.Html.pp ~indent:false ())
    html ;
  close_out fd

let document dir doc = List.iter (generate dir) @@ HDoc.all doc

let standard db dir =
  let static =
    Sys.readdir dir |> Array.to_list
    |> List.filter (fun x -> Filename.check_suffix x ".static.html")
    |> List.map (fun s -> Pages.static (Filename.concat dir s))
  in
  let iter url renderer gen =
    List.iter (fun x -> generate dir (url x, renderer x db)) (gen db)
  in
  LinearA.Script.DB.load (Filename.concat Sys.argv.(1) "signs.json") ;
  generate dir H.page ;
  iter Url.location Pages.location LinearA.Database.locations ;
  iter Url.period Pages.period LinearA.Database.periods ;
  iter Url.kind Pages.kind LinearA.Database.types ;
  List.iter (generate dir) static ;
  generate dir ("sign-list.html", Pages.sign_list) ;
  generate dir ("map.html", Pages.map) ;
  generate dir ("browse.html", Pages.browse db) ;
  generate dir (Pages.search_result LinearA.Document.ty) ;
  generate dir (Pages.search_result LinearA.Document.Sequence.ty) ;
  generate dir (Pages.search_result LinearA.Document.Attestation.ty) ;
  ()
