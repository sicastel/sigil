open Core
open Prelude

let run_command cmd =
  let ((_, _, stderr) as process) =
    Unix.open_process_full cmd (Unix.environment ())
  in
  let error = String.concat "\n" @@ lines stderr in
  match Unix.close_process_full process with
  | Unix.WEXITED 0 -> return ()
  | _ -> fail "Error when running command `%s': %s" cmd error

let run_command fmt = Printf.kprintf run_command fmt

type layer =
  {name: string; fname: string; properties: Importer.layer_properties}

let maindoc_file = "maindoc.xml"

let xml_parser = XmlParser.make ()

let _ = XmlParser.prove xml_parser false

let parse_string s = XmlParser.(parse xml_parser (SString s))

let element = function
  | Xml.Element (name, _, _) -> name
  | _ -> failwith "element"

let find_and_update pred list f =
  let rec aux acc = function
    | [] -> return None
    | t :: q when pred t ->
        let* t', result = f t in
        return @@ Some (List.rev acc @ (t' :: q), result)
    | t :: q -> aux (t :: acc) q
  in
  aux [] list

let update_child_by_name xml f name =
  let* kids, result =
    find_and_update
      (fun x -> String.lowercase_ascii (element x) = name)
      (Xml.children xml) f
    >>= function
    | Some x -> return x
    | None -> fail "Could not find child with name %s" name
  in
  match xml with
  | Xml.Element (name, attrs, _) ->
      return (Xml.Element (name, attrs, kids), result)
  | _ -> fail "Could not find element %s" name

let rec update_offstring xml f = function
  | [] -> return (f xml)
  | name :: names ->
      update_child_by_name xml
        (fun xml' -> update_offstring xml' f names)
        name

let get_offspring xml names =
  update_offstring xml (fun x -> (x, x)) names >|= snd

let _set_offspring xml names v =
  update_offstring xml (fun _ -> (v, ())) names >|= snd

let rec filter_layers ?(number = 1) ?(acc = []) = function
  | [] -> List.rev acc
  | Xml.Element ("layer", attrib, _) :: rest -> (
    try
      let name = List.assoc "name" attrib in
      match Importer.parse_properties number name with
      | Some properties ->
          filter_layers
            ~acc:
              ( {properties; name; fname = List.assoc "filename" attrib}
              :: acc )
            ~number:(number + 1) rest
      | _ -> filter_layers ~acc ~number rest
    with _ -> filter_layers ~acc ~number rest )
  | _ :: rest -> filter_layers ~number ~acc rest

let meta_in handle =
  let parse file =
    catch
      Zip.(fun f -> parse_string @@ read_entry handle (find_entry handle f))
      file
  in
  let* maindoc = parse maindoc_file in
  let* layers =
    let* layers = get_offspring maindoc ["image"; "layers"] in
    (* List.rev is important: In the XML layers are stored in the
       top-to-bottom but we want them in the bottom (first sign) to top *)
    return (filter_layers ((*List.rev @@ *) Xml.children layers))
  in
  return
  @@ List.sort
       (fun x y ->
         compare x.properties.Importer.number y.properties.Importer.number)
       layers

let meta filename =
  let* handle = catch Zip.open_in filename in
  let x = meta_in handle in
  Zip.close_in handle ; x

let layers f =
  match meta f with exception e -> Error (Printexc.to_string e) | x -> x

let export ~inp ~outp =
  run_command "krita --export --export-filename %s %s" (Filename.quote outp)
    (Filename.quote inp)

let smatch pattern s =
  try
    ignore @@ Str.search_forward (Str.regexp_string pattern) s 0 ;
    true
  with _ -> false

let export_layer ~inp ~outp layer =
  let kra = Filename.((dirname outp // "kra-") ^ basename outp ^ ".kra") in
  let handle_out = Zip.open_out kra in
  let handle_in = Zip.open_in inp in
  let remove_other_layers string =
    let test line =
      if
        (not @@ smatch "<layer " line)
        || smatch ("\"" ^ layer.fname ^ "\"") line
      then line
      else Str.global_replace (Str.regexp "visible=\"1") "visible=\"0" line
    in
    String.concat "\n" @@ List.map test @@ Str.split (Str.regexp "\n") string
  in
  let deal_with_entry entry =
    let string = Zip.read_entry handle_in entry in
    let final_string =
      match entry.Zip.filename with
      | "maindoc.xml" -> remove_other_layers string
      | _ -> string
    in
    Zip.add_entry final_string handle_out ~extra:entry.Zip.extra
      ~comment:entry.Zip.comment ~mtime:entry.Zip.mtime entry.Zip.filename
  in
  let () =
    List.iter deal_with_entry (Zip.entries handle_in) ;
    Zip.close_out handle_out ;
    Zip.close_in handle_in
  in
  let* () = export ~inp:kra ~outp in
  return @@ Unix.unlink kra

let properties {properties; _} = properties
