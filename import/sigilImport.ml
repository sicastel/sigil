open Core
open LinearA
open Prelude
open Cmdliner

let mtime file = try Unix.((stat file).st_mtime) with _ -> 0.

let more_recent f ~than = mtime than < mtime f

(* CLI options *)

let krita_binary =
  let doc = "Path to the krita binary." in
  Arg.(value & opt string "krita" & info ["--krita"] ~docv:"KRITA" ~doc)

let output_directory =
  let doc = "Directory where to output the SigLA database." in
  Arg.(
    required
    & opt (some string) None
    & info ["o"; "output-directory"] ~docv:"OUTPUT-DIRECTORY" ~doc)

let force_regen =
  let doc = "Force regeneration of the meta.json files." in
  Arg.(value & flag & info ["f"; "force-regen"] ~docv:"FORCE-REGEN" ~doc)

let input_directory =
  let doc = "Directory where the input krita files are stored." in
  Arg.(
    required
    & pos 0 (some string) None
    & info [] ~docv:"INPUT-DIRECTORY" ~doc)

let rec scan_directory pred dir =
  let aux entry =
    let file = dir // entry in
    if entry = "." || entry = ".." then []
    else if pred file then [file]
    else if Sys.is_directory file then scan_directory pred file
    else []
  in
  Sort.list @@ List.concat @@ Array.to_list @@ Array.map aux
  @@ Sys.readdir dir

module I = Importer.Make (Krita)

let generate_database force_regen input_directory output_directory =
  let () = Script.DB.load (input_directory // "signs.json") in
  let krita_files =
    scan_directory
      (fun fname -> Filename.extension fname = ".kra")
      input_directory
  in
  let total = List.length krita_files in
  let handle_krita_file i filename =
    enter
      (fun () -> I.import ~force_regen ~db_path:output_directory ~filename)
      () "[%d / %d] Importing %s" i total filename
  in
  let docs = mapi_only_successful_warning handle_krita_file krita_files in
  let db = Database.create ~docs in
  let () = Database.write_file (Database.create ~docs) output_directory in
  FileGen.standard db output_directory ;
  return ()

let info_sigla =
  Term.info ~doc:"Parse a SigLA database and outputs json files." "sigla"

let _ =
  Term.(
    eval
      ( const generate_database $ force_regen $ input_directory
        $ output_directory
      , info_sigla ))
