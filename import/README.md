# Importer

This part of Sigil deals with importing annotated image files
(describing documents) into the database. Concretely, it creates a
program, `sigil-import` that transforms a corpus of annotated images
to a database that the Web interface can use.

For now, only one format of image is allowed: krita files. They should
be annotated as follows:

- Title: Should be of the form `Kind/Location/Name`, for instance
  `Tablet/Knossos/KN 3`.
- Description should contain the size in centimeters, eg. `3.0x2.0x3.0`
- Signs that sigil will recognise must be drawn on different layers,
  named `Number:Sign Name:options` where `Number` is the number of the
  occurrence, `Sign name` is the reading of the sign name, and
  `options` can be any options.
  
The final software takes a set of krita files, plus a database of
signs, and exports and crops the images as well as generates json
files, and javascript files for use in the web interface.
