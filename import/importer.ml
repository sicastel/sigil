open Core
open LinearA
open Prelude

type att_label = {sign: string; unsure: bool; meta: Document.Metadata.t}
[@@deriving yojson]

let label_of_att reading (meta : Document.Metadata.t) =
  match reading with
  | Script.Unreadable | Unclassified -> None
  | Readable {instance; confidence} ->
      Some
        {unsure = not confidence; sign = Script.Instance.show instance; meta}

type layer_properties =
  { number: int
  ; sign_name: string
  ; erasure: bool
  ; ghost: bool
  ; custom_role: Role.t option
  ; starting: Bound.t option
  ; ending: Bound.t option }

let is_uppercase c = Char.uppercase_ascii c = c

let valid_name name =
  if name = "" || String.contains name '-' then false
  else if name = "damage" || name = "damages" then false
  else if
    String.length name > 1
    && is_uppercase name.[0]
    && (not @@ is_uppercase name.[1])
  then false
  else true

let parse_properties layer_number layer_name =
  let list = split ':' layer_name in
  if list = [] then None
  else
    let number, list =
      try
        let n = int_of_string (List.hd list) in
        (* if n <> layer_number then warning "Inferred number %d, distinct
           from supplied number %d" layer_number n ;*)
        (n, List.tl list)
      with _ -> (layer_number, list)
    in
    match list with
    | name :: properties when valid_name name -> (
      try
        if properties = [] && String.contains name ' ' then None
        else
          let init =
            { number
            ; sign_name = name
            ; ghost = false
            ; starting = None
            ; ending = None
            ; custom_role = None
            ; erasure = false }
          in
          let fold props = function
            | "ghost" -> {props with ghost = true}
            | "]start" -> {props with starting = Some Bound.Unsure}
            | "]-start" -> {props with starting = Some Bound.Not_here}
            | "start" -> {props with starting = Some Bound.Here}
            | "end" -> {props with ending = Some Bound.Here}
            | "end[" -> {props with ending = Some Bound.Unsure}
            | "end-[" -> {props with ending = Some Bound.Not_here}
            | "erasure" -> {props with erasure = true}
            | s -> (
              match Parsing.run Role.parse s with
              | Ok role -> {props with custom_role = Some role}
              | _ ->
                  warning "Ignored unknown layer property `%s'" s ;
                  props )
          in
          Some (List.fold_left fold init properties)
      with _ -> None )
    | _ -> None

module type Sig = sig
  type layer

  val layers : string -> layer list optional

  val export : inp:string -> outp:string -> unit optional

  val export_layer : inp:string -> outp:string -> layer -> unit optional

  val properties : layer -> layer_properties
end

let mtime file = try Unix.((stat file).st_mtime) with _ -> 0.

let more_recent f ~than = mtime than < mtime f

module Make (I : Sig) = struct
  type state =
    { bound_start: Bound.t
    ; current_word: Document.Attestation.t list
    ; occurrences: Document.Attestation.t list
    ; words: Document.Sequence.t list }

  let compute_words doc occurrences =
    let push_word ?(ending = Bound.Here) state =
      if state.current_word = [] then state
      else
        let (word : Document.Sequence.t) =
          { Document.seq_doc = doc
          ; index = List.length state.words
          ; bounds = (state.bound_start, ending)
          ; items = List.rev state.current_word }
        in
        { state with
          bound_start = Bound.Not_here
        ; words = word :: state.words
        ; current_word = [] }
    in
    let fold state ((occurrence : Document.Attestation.t), props) =
      let ignored = occurrence.erasure || occurrence.ghost in
      (* First, we need to check if we need to end the word BEFORE this sign *)
      let state =
        if
          (not ignored)
          && (props.custom_role <> None || props.starting <> None)
        then push_word state
        else state
      in
      let state =
        match props.starting with
        | Some bound_start -> {state with bound_start}
        | _ -> state
      in
      let role, state =
        match props.custom_role with
        | Some r -> (r, state)
        | _ when fst occurrence.role <> Role.Unknown ->
            (occurrence.role, state)
        | None when not ignored ->
            let role =
              ( Role.Syllabogram
                  (List.length state.current_word, List.length state.words)
              , Uncertainty.Sure )
            in
            ( role
            , { state with
                current_word = {occurrence with role} :: state.current_word
              } )
        | None -> (occurrence.role, state)
      in
      let state =
        {state with occurrences = {occurrence with role} :: state.occurrences}
      in
      match props.ending with
      | None -> state
      | Some ending -> push_word ~ending state
    in
    let state =
      List.fold_left fold
        { bound_start = Bound.Here
        ; current_word = []
        ; words = []
        ; occurrences = [] }
        occurrences
    in
    let state = push_word state in
    let occs = Array.of_list (List.rev state.occurrences) in
    let words = Array.of_list (List.rev state.words) in
    (occs, words)

  let read_meta filename =
    let json_file = Filename.chop_extension filename ^ ".json" in
    Document.Metadata.of_file json_file

  let ( let* ) = ( >>= )

  let import ~force_regen ~filename ~db_path =
    let* meta = read_meta filename in
    let* layers = I.layers filename in
    let name = meta.Document.Metadata.name in
    let total_path = db_path // Document.path_ name in
    let meta_file = total_path // "meta.json" in
    let previous_version =
      try
        Some (force @@ Document.of_yojson @@ Yojson.Safe.from_file meta_file)
      with
      | Sys_error _ -> None
      | e ->
          Prelude.warning "Unable to parse file %s: %s" meta_file
            (Printexc.to_string e) ;
          None
    in
    let regenerate () =
      let _ = Sys.command ("mkdir -p " ^ Filename.quote total_path) in
      let whole = total_path // (name ^ ".png") in
      let* () =
        if not @@ Sys.file_exists whole then
          I.export ~inp:filename ~outp:whole
        else return ()
      in
      let export_layer document layer =
        let properties = I.properties layer in
        let doit () =
          let* reading =
            Parsing.run Script.Reading.parse properties.sign_name
          in
          let previous_version =
            match previous_version with
            | Some (D {Document.signs; _}) -> (
              try Some signs.(Document.index signs properties.number)
              with _ -> None )
            | _ -> None
          in
          let role =
            match reading with
            | Script.Readable {instance = {sign; _}; _} ->
                Script.Sign.default_role sign
            | _ -> (Role.Unknown, Uncertainty.Unsure)
          in
          let pic =
            db_path // Document.Attestation.image_ name properties.number
          in
          let () =
            let label =
              db_path // Document.Attestation.label_ name properties.number
            in
            Option.iter
              (fun x -> Yojson.Safe.to_file label (att_label_to_yojson x))
              (label_of_att reading meta)
          in
          let* position =
            match previous_version with
            | Some {Document.position; _} when Sys.file_exists pic ->
                return position
            | _ ->
                warning "Re-generating image" ;
                let tmp =
                  Filename.((dirname pic // "layer-") ^ basename pic)
                in
                let* () = I.export_layer ~inp:filename ~outp:tmp layer in
                let* r = Image.crop ~inp:tmp ~outp:pic in
                Unix.unlink tmp ; return r
          in
          let occurrence =
            { Document.att_doc = document
            ; reading
            ; position
            ; ghost = properties.ghost
            ; number = properties.number
            ; att_comment =
                ( try List.assoc properties.number meta.occ_comments
                  with _ -> None )
            ; erasure = properties.erasure
            ; role }
          in
          return (occurrence, properties)
        in
        enter doit () "In layer [%d/%d] [name: %s]" properties.number
          (List.length layers) properties.sign_name
      in
      let* document =
        fix_opt (fun document ->
            let occurrences =
              map_only_successful_warning (export_layer document) layers
            in
            let occurrences =
              List.sort
                (fun (o1, _) (o2, _) -> compare o1.Document.number o2.number)
                occurrences
            in
            let signs, sequences =
              enter (compute_words document) occurrences
                "Computing sign-sequences"
            in
            let* image_size = Image.size whole in
            return
              (Document.D
                 { Document.metadata = meta
                 ; image_size
                 ; path = Document.path_ name
                 ; signs
                 ; sequences } ) )
      in
      FileGen.document db_path document ;
      return document
    in
    match previous_version with
    | Some doc when more_recent meta_file ~than:filename && not force_regen
      ->
        return doc
    | _ ->
        if not force_regen then
          fail "Cannot read file -- pass -f to regenerate"
        else
          regenerate ()
          >>= fun doc ->
          Yojson.Safe.to_file meta_file @@ Document.to_yojson doc ;
          return doc
end
