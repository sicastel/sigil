open Core.Prelude
open Images

let crop ~inp ~outp =
  match Png.load inp [] with
  | Images.Rgba32 img -> (
      let open Rgba32 in
      let x = ref img.width
      and y = ref img.height
      and w = ref 0
      and h = ref 0 in
      for i = 0 to img.width - 1 do
        for j = 0 to img.height - 1 do
          if (get img i j).alpha > 50 then (
            x := min !x (i - 5) ;
            w := max !w (i + 5) ;
            y := min !y (j - 5) ;
            h := max !h (j + 5) )
        done
      done ;
      let x = max !x 0
      and y = max !y 0
      and w = min (!w - !x) (img.width - !x)
      and h = min (!h - !y) (img.height - !y) in
      Printf.eprintf "      (X:%d Y:%d W:%d H:%d)\n%!" x y w h ;
      try
        let img' = Rgba32.sub img x y w h in
        Png.save outp [] (Rgba32 img') ;
        return {Core.Rect.x; y; w; h}
      with _ -> fail "Unable to compute boundary (empty image?)" )
  | _ | (exception _) -> fail "Unable to load png file: %s" inp

let size file =
  Core.Prelude.catch
    (fun () ->
      let _, {header_width; header_height; _} = file_format file in
      (header_width, header_height))
    ()
