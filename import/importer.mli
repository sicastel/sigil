open Core
open LinearA
open Prelude

type layer_properties =
  { number: int
  ; sign_name: string
  ; erasure: bool
  ; ghost: bool
  ; custom_role: Role.t option
  ; starting: Bound.t option
  ; ending: Bound.t option }

val parse_properties : int -> string -> layer_properties option

module type Sig = sig
  type layer

  val layers : string -> layer list optional

  val export : inp:string -> outp:string -> unit optional

  val export_layer : inp:string -> outp:string -> layer -> unit optional

  val properties : layer -> layer_properties
end

module Make (I : Sig) : sig
  val import :
       force_regen:bool
    -> filename:string
    -> db_path:string
    -> Document.t optional
end
