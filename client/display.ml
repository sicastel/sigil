open Js_of_ocaml
open Core
open LinearA

let compute_height occ =
  let open Document in
  (occ.position.y + (occ.position.h / 2))
  * 100
  / snd (Document.image_size (Document.Attestation.document occ))

let sign_info = H.div []

let reset_sign_info () = H.set_content sign_info []

let set_sign_info ~cl left close content height =
  H.set_classes sign_info
    ( "document-popup"
    :: (if left then "document-popup-left" else "document-popup-right")
    :: cl ) ;
  let top, unit =
    match height with
    | `Absolute px ->
        ( px
          - (Dom_html.getElementById "document-view")##.offsetTop
          + int_of_float
              (Js.float (Js.Unsafe.js_expr "window.scrollY" : float))
        , "px" )
    | `Percent pc ->
        H.add_class sign_info "document-popup-selected" ;
        (pc, "%")
  in
  let close_button =
    match height with
    | `Absolute _ -> []
    | `Percent _ -> [Service.Link.a ~cl:"close-button" close [H.txt " × "]]
  in
  (H.cast sign_info)##.style##.top := Js.string (string_of_int top ^ unit) ;
  H.set_content sign_info (close_button @ content)

let svg_set_info ~cl left close content =
  let on_move ev =
    set_sign_info ~cl left close content (`Absolute ev##.clientY) ;
    true
  in
  S.[a_onmousemove on_move]

module Show = struct
  module Bounds = struct
    let show (a, b) x =
      (H.txt_ (Bound.show_before a) :: x) @ [H.txt_ (Bound.show_after b)]
  end

  module Reading = struct
    let show show_inst = function
      | Script.Unreadable -> [H.txt "[?]"]
      | Unclassified -> [H.span ~cl:["unclassified"] [H.txt "[unclassified]"]]
      | Readable {instance; confidence} ->
          let data = show_inst instance in
          if confidence then [H.span ~cl:["sure-reading"] data]
          else [H.span ~cl:["unsure-reading"] data]
  end

  module Composition = struct
    open Composition

    let show show_data (x, bounds) =
      let rec aux = function
        | Simple x -> show_data x
        | Composed ls ->
            H.concat' (fun () -> [H.txt " + "]) @@ List.map (show true) ls
        | TightJuxtaposition ls ->
            H.concat' (fun () -> [H.txt " | "]) @@ List.map (show true) ls
        | LooseJuxtaposition ls ->
            H.concat' (fun () -> [H.txt " ∥ "]) @@ List.map (show true) ls
        | Fuse (a, b) ->
            show false a @ [H.txt " + {"] @ show false b @ [H.txt "}"]
      and show b x =
        match (b, x) with
        | _, Simple x -> show_data x
        | false, x -> aux x
        | true, x -> [H.txt "("] @ aux x @ [H.txt ")"]
      in
      Bounds.show bounds (aux x)
  end

  module PhoneticValue = struct
    open PhoneticValue

    let show {value; annot} =
      [H.txt "%s" value]
      @ match annot with Some x -> [H.sub [H.txt_ x]] | None -> []
  end

  module Sign = struct
    let image ~cl sign =
      let make_img src =
        [ H.img
            ~a:
              [ H.a_class [cl]
              ; H.a_title (Printf.sprintf "Sign %s" (Script.Sign.show sign))
              ]
            ~src
            ~alt:(Printf.sprintf "Sign %s" (Script.Sign.show sign))
            () ]
      in
      match Database.representative_image_sign sign with
      | Some src -> make_img src
      | None -> (
        try make_img @@ snd @@ List.hd sign.variants with _ -> [] )

    let show_formal t =
      let x = t.Script.prefix ^ Printf.sprintf "%02d" t.Script.number in
      if t.vessel then H.[H.txt_ x; sup [H.txt "VAS"]] else [H.txt_ x]

    let show_syllabogram t =
      match t.Script.phonetic with
      | Some x -> PhoneticValue.show x
      | _ when t.prefix = "AB" -> [H.txt "*%02d" t.number]
      | _ -> show_formal t

    let show_logogram t =
      match t.Script.latin with
      | Some x -> show_formal t @ [H.txt_ "/"; H.txt_ x]
      | _ -> show_formal t

    let miniature sign =
      match image ~cl:"sign-image-nice" sign with
      | [] -> show_formal sign
      | l -> [Widget.inline_figure ~caption:(show_formal sign) l]

    let show_image_or_formal ~cl sign =
      match image ~cl sign with [] -> show_formal sign | l -> l
  end

  module Instance = struct
    let show show_meta {Script.sign; variant} =
      let m = show_meta sign in
      match variant with Some x -> m @ [H.sup [H.txt_ x]] | _ -> m

    let image ?(cl = "") inst =
      let make_img src =
        [ H.img
            ~a:
              [ H.a_class [cl]
              ; H.a_title
                  (Printf.sprintf "Sign %s" Script.(Sign.show inst.sign)) ]
            ~src
            ~alt:(Printf.sprintf "Sign %s" Script.(Sign.show inst.sign))
            () ]
      in
      match Database.representative_image_inst inst with
      | None -> Sign.image ~cl inst.sign
      | Some s -> make_img s

    let miniature sign =
      match image ~cl:"sign-image-nice" sign with
      | [] -> show Sign.show_formal sign
      | l -> [Widget.inline_figure ~caption:(show Sign.show_formal sign) l]
  end

  module Attestation = struct
    let show occ =
      Reading.show
        ( match occ.Document.role with
        | Role.Logogram, _ -> Instance.show Sign.show_logogram
        | Role.Syllabogram (_, _), _ -> Instance.show Sign.show_syllabogram
        | _ -> Instance.show Sign.show_formal )
        occ.reading
  end

  module Sequence = struct
    let show (word : Document.Sequence.t) =
      let signs =
        List.map
          (fun {Document.number; _} ->
            Document.at (Document.Sequence.document word) number)
          word.Document.items
      in
      [H.txt_ (Bound.show_before (fst word.bounds))]
      @ H.concat
          (fun () -> H.txt "-")
          (List.concat @@ List.map Attestation.show signs)
      @ [H.txt_ (Bound.show_after (snd word.bounds))]
  end

  let decomposition ?(cl = "composition") ?(prefix = "") ?(suffix = "") si =
    match Script.composition si with
    | None -> []
    | Some c ->
        [ H.span ~cl:[cl]
            ( H.txt "%s" prefix
              :: Composition.show (Reading.show Instance.miniature) c
            @ [H.txt "%s" suffix] ) ]
end

module Attestation = struct
  open Script
  open Document

  let info occurrence =
    let _composition =
      match Document.Attestation.sign occurrence with
      | None -> []
      | Some x -> (
        match Show.decomposition x with [] -> [] | l -> H.txt "Dec. " :: l )
    in
    [ H.span
        ~a:[H.a_class ["info-title"]]
        (H.txt "#%d: " occurrence.number :: show ~menu:true occurrence)
    ; H.span ~a:[H.a_class ["role"]] [H.txt "%s" (function_of occurrence)]
    ; image occurrence
    ; Widget.show_note
        (fun s k ->
          Socket.ask
            (Protocol.EditOccurrenceComment
               ( Document.Attestation.document_name occurrence
               , s
               , occurrence.number ))
            k)
        occurrence.att_comment ]

  let widget ?(popup_style = `Floating) selected occurrence =
    let set f =
      f ~cl:(class_of occurrence)
        (Document.Attestation.is_left occurrence)
        (Service.Link.document (Attestation.document occurrence))
        (info occurrence)
    in
    let attrs =
      match popup_style with
      | `Floating -> set svg_set_info
      | `Fixed ->
          if selected then
            set set_sign_info (`Percent (compute_height occurrence)) ;
          []
    in
    S.a
      ~a:[Service.Link.(s_href (attestation occurrence))]
      [ S.rect ~a:attrs
          ( ["sign"]
          @ (if selected then ["selected"] else [])
          @ class_of occurrence )
          occurrence.Document.position ]

  let miniature (occ : Document.Attestation.t) =
    Widget.figure
      ~caption:
        ( [ Service.Link.(
              text
                (document (Document.Attestation.document occ))
                (Document.Attestation.document_name occ))
          ; H.txt ": " ]
        @ show ~menu:true occ )
      [Service.Link.(a (attestation occ) [image occ])]
end

module Sequence = struct
  open LinearA.Document
  open Sequence

  let show_bare word = Show.Sequence.show word

  let show ?(menu = false) (wo : Sequence.t) =
    let document : Document.t = document wo in
    if not menu then show_bare wo
    else
      [ Widget.menu
          [Service.Link.(a (word wo)) (show_bare wo)]
          [ Service.Link.text
              Service.(
                link search_results
                  (Search.word ~exp:(SequencePattern.of_word wo) ()))
              "View all occurrences of this word"
          ; Service.Link.text
              Service.(
                link search_results
                  (Search.word
                     ~location:[Document.location document]
                     ~exp:(SequencePattern.of_word wo)
                     ()))
              ( "View all occurrences of this word at "
              ^ Document.location document ) ] ]

  let info word =
    [H.span ~cl:["info-title"] [H.txt "Sequence #%d" (1 + word.index)]]
    @ show ~menu:true word
    @ [H.txt " (%d signs)" (List.length word.items)]

  let rect_of_word popup_style selected (wo : Sequence.t) =
    let set f =
      f ~cl:["word"]
        (Document.Sequence.is_left wo)
        (Service.Link.document ~wordview:true (Sequence.document wo))
        (info wo)
    in
    let attrs =
      match popup_style with
      | `Floating -> set svg_set_info
      | `Fixed when selected ->
          set set_sign_info (`Percent (compute_height (List.hd wo.items))) ;
          []
      | _ -> []
    in
    let rects = List.map (fun o -> o.Document.position) wo.items in
    let rects = List.map (S.rect []) @@ Rect.bounds rects in
    S.(
      a
        ~a:
          ( attrs
          @ [ Service.Link.(s_href (word wo))
            ; a_class
                ( [(if wo.index mod 2 = 0 then "even" else "odd")]
                @ if selected then ["word"; "selected"] else ["word"] ) ] )
        rects)

  let widget ?(popup_style = `Floating) selected word =
    rect_of_word popup_style selected word
end

module Document = struct
  type size = [`Real_size of float | `Full_width]

  let size_of doc zoom =
    match (zoom, Document.size doc) with
    | None, _ -> ((100., Some `Percent), (100., Some `Percent))
    | _, None -> ((2., Some `Cm), (100., Some `Percent))
    | Some z, Some sz ->
        ((z *. Size.width sz, Some `Cm), (z *. Size.height sz, Some `Cm))

  let make_svg ?zoom doc widgets =
    let path = Document.image doc in
    let id = H.fresh_id () in
    let draw () =
      H.make_svg ~a:[S.a_id id] path ~size:(size_of doc zoom)
        ~image_size:(Document.image_size doc) widgets
    in
    draw ()

  (* let make_transcription doc = let open Rect in let r = {x = 0; y = 0; w =
     fst doc.image_size; h = snd doc.image_size} in let make_text occurrence
     = S.html occurrence.Document.position (H.span ~a:[H.a_class
     ["transcription-sign"]] (Attestation.show doc occurrence)) in [ H.svg r
     ((100., Some `Percent), (100., Some `Percent)) (List.map make_text @@
     Array.to_list doc.signs) ]*)

  let miniature doc =
    Service.Link.(a (document doc))
      [ Widget.figure
          ~caption:[H.txt_ (Document.name doc)]
          [make_svg ~zoom:1. doc []] ]

  let svg_with_signs ?popup_style ?selected doc =
    [ sign_info
    ; make_svg doc
        (List.map
           (fun o ->
             Attestation.widget ?popup_style
               (selected = Some o.Document.number)
               o)
           (Array.to_list (Document.signs doc))) ]

  let svg_with_words ?popup_style ?selected doc =
    [ sign_info
    ; make_svg doc
        (List.map
           (fun w ->
             Sequence.widget ?popup_style
               (selected = Some w.Document.index)
               w)
           (Array.to_list (Document.sequences doc))) ]

  let miniature_with_word wo =
    Service.Link.(a (word wo))
      [ Widget.figure
          ~caption:
            ( [H.txt "%s" (Document.Sequence.document_name wo); H.txt ": "]
            @ Sequence.show_bare wo )
          [ make_svg ~zoom:1.
              (Document.Sequence.document wo)
              [Sequence.widget true wo] ] ]

  let miniature_with_occurrence occ =
    Service.Link.(a ~cl:"miniature" (attestation occ))
      [ make_svg ~zoom:1.
          (Document.Attestation.document occ)
          [Attestation.widget true occ]
      ; H.span
          ~a:[H.a_class ["caption"]]
          ( H.txt "%s: " (Document.Attestation.document_name occ)
          :: Attestation.show occ ) ]
end

module Grouping = struct
  let show_ty_short : type a. a Ty.t -> a -> string = Ty.Methods.show

  let show_ty_long :
      type a. a Ty.t -> a Search.Settings.t -> a -> _ H.elt list =
   fun ty settings ->
    let module Matcher = Ty.Matcher (struct
      type 'a t = 'a -> Html_types.div_content H.elt list
    end) in
    match
      Matcher.(
        run
          [ make LinearA.Document.ty (fun x -> [Document.miniature x])
          ; make LinearA.Document.Sequence.ty (fun x ->
                [Document.miniature_with_word x])
          ; make LinearA.Document.Attestation.ty (fun x ->
                match settings with
                | Attestation {whole = true} ->
                    [Document.miniature_with_occurrence x]
                | _ -> [Attestation.miniature x]) ])
        ty
    with
    | Ok v -> v
    | Error _ -> fun _ -> []

  let display n title content =
    if n = 0 then
      H.fieldset ~a:[H.a_class ["result1"]] ~legend:(H.legend title) content
    else
      H.fieldset ~a:[H.a_class ["result"]] ~legend:(H.legend title) content

  let rec show_aux n db rtyp settings = function
    | Grouping.Group (name, typ, groups) ->
        let (U {friendly; _}) = Builtins.find_by_name_untyped name in
        List.map
          (fun (key, l) ->
            display n
              [ H.txt "%s: %s [%d results]" friendly (show_ty_short typ key)
                  (Grouping.length l) ]
              (show_aux (n + 1) db rtyp settings l))
          groups
    | Grouping.List l ->
        List.concat @@ List.map (show_ty_long rtyp settings) l

  let show db rtyp settings g =
    let l = Grouping.length g in
    H.txt "There are %d results to your query." l
    :: H.br ()
    :: show_aux 0 db rtyp settings g
end

let from_optional = function
  | Ok v -> v
  | Error e ->
      H.Notification.error "%s" e ;
      failwith e
