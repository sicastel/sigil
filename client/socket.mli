val ask :
  'a LinearA.Protocol.question -> ('a Core.Prelude.optional -> unit) -> unit

val load : unit -> unit

val db : unit -> LinearA.Database.t

(*val perform : unit LinearA.Protocol.question -> unit*)

val is_logged_in : unit -> bool

val login : string -> unit
