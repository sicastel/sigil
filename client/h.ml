open Core
open Js_of_ocaml

let fresh_id =
  let r = ref 0 in
  fun () ->
    incr r ;
    "_" ^ string_of_int !r

open Js_of_ocaml_tyxml
include Tyxml_js.Html

let mklink ?(onclick = fun _ -> ()) href descr =
  a ~a:[a_href href; a_onclick (fun _ -> onclick () ; false)] descr

let raw s =
  let open Tyxml_js in
  let div = Dom_html.document##createElement (Js.string "div") in
  div##.innerHTML := Js.string s ;
  Of_dom.of_div div

let raw_span s : [> Html_types.span] elt =
  let open Tyxml_js in
  let div = Dom_html.document##createElement (Js.string "span") in
  div##.innerHTML := Js.string s ;
  Obj.magic @@ Of_dom.of_div div

let empty = Unsafe.data ""

let when_def x f = match x with None -> empty | Some a -> f a

let guard b e = if b then e else empty

let guard' b e = if b then e else []

let fix f =
  let rec x = lazy (f x) in
  Lazy.force x

let concat sep l =
  let rec aux = function
    | [] -> []
    | [t] -> [t]
    | t :: q -> t :: sep () :: aux q
  in
  aux (List.filter (fun x -> x <> empty) l)

let concat' sep l =
  let rec aux = function
    | [] -> []
    | [t] -> [t]
    | t :: q -> t :: sep () :: aux q
  in
  List.concat @@ aux (List.filter (fun x -> x <> []) l)

let unless b e = if not b then e else empty

let txt_ = txt

let txt fmt = Printf.kprintf txt fmt

let svg ?(a = []) {Rect.x; y; w; h} (sx, sy) =
  svg
    ~a:
      ( [ S.a_width sx
        ; S.a_height sy
        ; S.a_viewBox (float x, float y, float w, float h) ]
      @ a )

let action ?(cl = "action") f text =
  a ~a:[a_class [cl]; a_onclick (fun _ -> f () ; false)] text

let contents x = x##.childNodes

let cast = To_dom.of_element

let uncast = Of_dom.of_element

let cast_textarea = To_dom.of_textarea

let set_content x (content : 'a elt list) =
  set_content_node (cast x) (List.map cast content)

let set_content_id x content =
  set_content_node (Dom_html.getElementById x) (List.map cast content)

let set_class x cl = (cast x)##.className := Js.string cl

let add_class x cl = (cast x)##.classList##add (Js.string cl)

let set_classes x cl =
  set_class x "" ;
  List.iter (add_class x) cl

let remove_class x cl = x##.classList##remove (Js.string cl)

module Notification = struct
  let notification = div []

  let erase () = set_content notification []

  let notification cl content =
    set_class notification cl ;
    set_content notification content

  let error fmt = Printf.kprintf (fun s -> notification "error" [txt_ s]) fmt

  let success fmt =
    Printf.kprintf (fun s -> notification "success" [txt_ s]) fmt

  let warning fmt =
    Printf.kprintf (fun s -> notification "warning" [txt_ s]) fmt
end

let two_columns ?(a' = []) ?(b' = []) a b =
  div ~a:[a_class ["two-columns"]] [div ~a:a' a; div ~a:b' b]

let make_svg ?a path ~size:(w, h) ~image_size widgets =
  let r = {Rect.x = 0; y = 0; w = fst image_size; h = snd image_size} in
  svg ?a r (w, h) (S.image path r :: widgets)

let div ?(cl = []) ?(a = []) elem =
  div ~a:(a @ if cl = [] then [] else [a_class cl]) elem

let span ?(cl = []) ?(a = []) elem =
  span ~a:(a @ if cl = [] then [] else [a_class cl]) elem

let map (image_alt, image) areas =
  let id = fresh_id () in
  div ~cl:["interactive-map"]
    [ img ~src:image ~alt:image_alt ~a:[Obj.magic @@ a_usemap ("#" ^ id)] ()
    ; map ~a:[a_name id] areas ]

let area ?(shape = `Rect) ?(href = "") ?(on_in = fun _ -> true)
    ?(on_out = fun _ -> true) ?(on_click = fun _ -> true) ~alt r data =
  area ~alt
    ~a:
      [ Obj.magic @@ a_href href
      ; a_shape shape
      ; a_coords [r.Core.Rect.x; r.Core.Rect.y; r.Core.Rect.w; r.Core.Rect.h]
      ; a_onmouseover on_in
      ; a_onmouseout on_out
      ; a_onclick on_click ]
    data

let combobox default on_change groups =
  let create self =
    let on_change _ =
      let value =
        Js.to_string (Tyxml_js.To_dom.of_select (Lazy.force self))##.value
      in
      List.iter
        (fun (x, l) ->
          List.iter (fun (y, z) -> if y = value then on_change x y z) l )
        groups ;
      false
    in
    let make_option _ (s, _) = option ~a:[a_value s] (txt_ s) in
    let make_group (group_name, list) =
      optgroup ~label:group_name (List.map (make_option group_name) list)
    in
    select
      ~a:[a_required (); a_onchange on_change]
      (option
         ~a:[a_value ""; a_disabled (); a_selected (); a_hidden ()]
         (txt_ default)
       :: List.map make_group groups )
  in
  fix create

module Dom = struct
  include Tyxml_js.To_dom

  let set_content elem content =
    let elem = of_element elem in
    elem##.innerHTML := Js.string "" ;
    List.iter (fun child -> Dom.appendChild elem (of_element child)) content
end

let enclosed title content = fieldset ~legend:(legend title) content
