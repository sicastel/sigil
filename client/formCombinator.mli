open Core.Prelude
open Model.M.Html

(** A form producing values of type ['a] *)
type 'a t

type block = Html_types.div_content elt

type inline = Html_types.span_content elt

val return : ?block:block list -> 'a -> 'a t
(** The empty form that returns always the argument *)

val empty : unit t

val prefix' : (unit -> block list) -> 'a t -> 'a t
(** Note that the closure around block list is important due to blocks being
    not copiable. *)

val prefix : string -> 'a t -> 'a t

val update_html : (block list -> block list) -> 'a t -> 'a t

val bimap :
  forward:('a -> 'b optional) -> backward:('b -> 'a optional) -> 'a t -> 'b t

val bimap_pure : forward:('a -> 'b) -> backward:('b -> 'a) -> 'a t -> 'b t

val ( *** ) : ?sep:block list -> 'a t -> 'b t -> ('a * 'b) t

val block : block list -> unit t

val list : ?sep:block list -> 'a t list -> 'a list t

val repeat : ?numbered:bool -> string -> 'a t -> 'a list t

(** Choices *)

type 'a choice =
  [`Placeholder of string | `Default of 'a]
  * (string * (string * 'a) list) list

val choose : ?show:bool -> 'a choice -> 'a t

val choose_then :
  inverse:('b -> 'a option) -> 'a choice -> ('a -> 'b t) -> 'b t

(** base types *)

val text :
  ?placeholder:string -> ?block:inline list -> 'a Core.Conv.t -> 'a t

val bool : ?default:bool -> ?block:inline list -> unit -> bool t

val subset : string list -> string list t

val autocomplete :
     'a Core.Conv.t
  -> uri
  -> (string -> Widget.Autocomplete.candidate list)
  -> 'a t

(** Running form *)
type 'a instance

val create : 'a option -> 'a t -> 'a instance

val get : 'a instance -> 'a option

val draw : 'a instance -> block list

val run :
     cl:Html_types.nmtokens
  -> button:string
  -> 'a t
  -> 'a option
  -> ('a -> unit)
  -> block list
