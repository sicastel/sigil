open Core
open LinearA
open Js_of_ocaml

type 'param _t =
  { conv: 'param Conv.t
  ; name: string
  ; descr: 'param -> string
  ; mutable handle: 'param -> unit }

type t = Service : 'param _t -> t

let services = ref []

let create ?descr conv name =
  let service =
    { conv
    ; name
    ; handle = (fun _ -> ())
    ; descr = (match descr with Some f -> f | _ -> conv.show) }
  in
  services := Service service :: !services ;
  service

let create_const name = create Conv.unit name

let set_handler service f = service.handle <- f

let home = create_const ""

let documents = create_const "documents"

let signs = create_const "signs"

let maps = create_const "maps"

let login = create Conv.string "login"

let location = create Conv.string "location"

let period = create Conv.string "period"

let kind = create Conv.string "type"

(* Viewing stuff *)
let view_document =
  let show (name, doc_view, index) =
    match (doc_view, index) with
    | false, None -> name
    | false, Some i -> name ^ "/" ^ string_of_int i
    | true, None -> name ^ ":word"
    | true, Some i -> name ^ ":word/" ^ string_of_int i
  in
  let parse =
    let open Parsing in
    let* name = take_while (fun c -> c <> '/' && c <> ':') in
    let* word_view =
      string ":word" >> return true <|> (string "" >> return false)
    in
    let* id = option (keyword "/" >> int) in
    return (name, word_view, id)
  in
  create (Conv.make ~show ~parse) "doc"

let view_sign = create Script.Sign.conv "sign"

(* Searching *)

let search_results = create Search.conv "search-result"

let search = create Search.conv "search"

let search_sign = create Conv.unit "search-sign"

let search_sequence = create Conv.unit "search-sequence"

let search_inscription = create Conv.unit "search-inscription"

let browse = create Conv.unit "browse"

let format_anchor service param =
  "#" ^ service.name ^ ":" ^ service.conv.show param

let hooks = ref []

let add_hook f = hooks := f :: !hooks

let run service param =
  Dom_html.window##.location##.hash
  := Js.encodeURI @@ Js.string (format_anchor service param) ;
  service.handle param

module Link = struct
  type l = L : 'a _t * 'a -> l

  let h_href (L (service, param)) =
    H.a_href @@ Js.to_string @@ Js.encodeURI
    @@ Js.string (format_anchor service param)

  let s_href (L (service, param)) =
    S.a_href @@ Js.to_string @@ Js.encodeURI
    @@ Js.string (format_anchor service param)

  let a ?(cl = "") link data = H.a ~a:[H.a_class [cl]; h_href link] data

  let run (L (service, data)) = run service data

  let button data link =
    H.span
      ~a:
        [ H.a_class ["button"]
        ; H.a_onclick (fun _ ->
              run (link ()) ;
              true ) ]
      data

  let text link s = a link [H.txt "%s" s]

  let document_ ?(wordview = false) doc =
    L (view_document, (doc, wordview, None))

  let document ?wordview doc = document_ ?wordview (Document.name doc)

  let attestation_ doc number = L (view_document, (doc, false, Some number))

  let attestation occ =
    attestation_ (Document.Attestation.document_name occ) occ.Document.number

  let word_ doc number = L (view_document, (doc, true, Some number))

  let word word =
    word_ (Document.Sequence.document_name word) word.Document.index

  let instance i =
    L (search_results, Search.(sign ~sign:(pattern_of_instance i)) ())
end

let link a b = Link.L (a, b)

let execute_anchor () =
  let anchor =
    Js.to_string @@ Js.decodeURI @@ Dom_html.window##.location##.hash
  in
  let anchor =
    if anchor = "" then ""
    else if anchor.[0] = '#' then
      String.sub anchor 1 (String.length anchor - 1)
    else anchor
  in
  let name, param =
    try Scanf.sscanf anchor "%[^:]:%[^\n]" (fun name param -> (name, param))
    with _ -> ("", "")
  in
  let _ = List.iter (fun f -> f ()) !hooks in
  match List.find_opt (fun (Service serv) -> serv.name = name) !services with
  | None ->
      Printf.eprintf "Unknown service %s%!" name ;
      run home ()
  | exception e ->
      Printf.eprintf "Exception during computation %s%!"
        (Printexc.to_string e) ;
      run home ()
  | Some (Service ({conv; _} as service)) -> (
    match Conv.parse conv param with
    | Ok data -> (
      try service.handle data
      with e ->
        H.Notification.error "When loading the page: %s"
          (Printexc.to_string e) )
    | Error err ->
        Printf.eprintf "Invalid data `%s' for service %s: %s%!" param name
          err )

let _link_obj (type a) text (ty : a Ty.t) (data : a) =
  match Ty.test ty Script.Sign.ty with
  | Ok Refl -> Link.a (link view_sign data) text
  | _ -> (
    match Ty.test ty Document.Attestation.ty with
    | Ok Refl -> Link.a (Link.attestation data) text
    | _ -> (
      match Ty.test ty Document.Sequence.ty with
      | Ok Refl -> Link.a (Link.word data) text
      | _ -> (
        match Ty.test ty Document.ty with
        | Ok Refl -> Link.a (Link.document data) text
        | _ -> failwith "" ) ) )

let link_obj link (Ty.Object.O (ty, data)) = _link_obj link ty data
