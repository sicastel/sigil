open Js_of_ocaml

type state =
  { hidden: bool
  ; owner: Dom_html.element Js.t option
  ; content: Html_types.div_content H.elt list }

let status = ref {hidden = true; owner = None; content = []}

let update st =
  try
    status := st ;
    H.set_content "tooltip" !status.content ;
    (H.get "tooltip")##.style##.display
    := Js.string (if !status.hidden then "none" else "block")
  with _ -> ()

let mouse_on content _ =
  if !status.owner = None then update {!status with hidden = false; content} ;
  false

let mouse_out _ =
  if !status.owner = None then
    update {!status with hidden = true; content = []} ;
  false

let click content ev =
  update
    { hidden = false
    ; owner = Some (Js.Opt.get ev##.target (fun _ -> failwith ""))
    ; content } ;
  false

let setup () =
  Dom_html.window##.onmousemove
  := Dom.handler (fun e ->
         if !status.owner = None then (
           let x = e##.clientX and y = e##.clientY in
           let new_width =
             let proposal = x + 20 in
             if
               proposal + (H.get "tooltip")##.clientWidth
               >= Js.Optdef.get Dom_html.window##.innerWidth (fun _ ->
                      failwith "")
             then x - 20 - (H.get "tooltip")##.clientWidth
             else proposal
           in
           let new_height =
             let proposal = y - ((H.get "tooltip")##.clientHeight / 2) in
             if
               proposal + (H.get "tooltip")##.clientHeight
               >= Js.Optdef.get Dom_html.window##.innerHeight (fun _ ->
                      failwith "")
             then y - 20 - (H.get "tooltip")##.clientHeight
             else proposal
           in
           (H.get "tooltip")##.style##.top
           := Js.string (Printf.sprintf "%dpx" new_height) ;
           (H.get "tooltip")##.style##.left
           := Js.string (Printf.sprintf "%dpx" new_width) ) ;
         Js._false) ;
  Dom_html.window##.onmousedown
  := Dom.handler (fun ev ->
         if
           !status.owner
           <> Some (Js.Opt.get ev##.target (fun _ -> failwith ""))
         then update {content = []; hidden = true; owner = None} ;
         Js._true) ;
  update !status

let html content =
  H.[a_onmouseover (mouse_on content); a_onmouseout mouse_out]

let svg content =
  S.[a_onmouseover (mouse_on content); a_onmouseout mouse_out]
