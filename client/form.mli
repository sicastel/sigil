open Core
open Model.M.Html

val ask_search :
     'a Ty.t
  -> 'a LinearA.Search.t option
  -> ('a LinearA.Search.t -> unit)
  -> FormCombinator.block list_wrap
