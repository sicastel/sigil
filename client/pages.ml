open Core
open LinearA
open Prelude

let fill_page ?(sidebar = true) ?(home = false) ?(no_erase = false) ?subtitle
    tit content =
  H.set_content_id "app"
    ( [ H.div ~a:[H.a_id "title"] tit
      ; H.when_def subtitle (fun s -> H.div ~a:[H.a_id "subtitle"] s)
      ; H.div ~a:[H.a_id "notification"] [] ]
    @ content ) ;
  if not @@ no_erase then H.Notification.erase () ;
  if sidebar then QuickSearch.setup "side-search" ;
  H.set_class
    (H.uncast Js_of_ocaml.Dom_html.document##.body)
    (if home then "home" else "")

let make_group typ (name, gkind) =
  let open Builtins in
  match List.find (fun (B b) -> b.name = name) (of_typ typ) with
  | B b ->
      let* param = Ty.(cast unit b.param ()) in
      return @@ Grouping.D (b.name, b.ret, b.eval param, gkind)

let make_groups typ list =
  sequence_only_successful @@ List.map (make_group typ) list

module Home = struct
  let display () =
    let open H in
    let quick = QuickSearch.create () in
    fill_page ~home:true
      ~subtitle:[txt "The signs of Linear A: a paleographical database"]
      [txt "SigLA"]
      [ div ~a:[a_id "home"]
          [ quick
          ; H.span ~cl:["home-search"] [H.txt "Search"]
          ; H.span ~cl:["sep"] []
          ; H.span ~cl:["home-browse"] [H.txt "Explore"]
          ; Service.Link.a ~cl:"home-search-sign"
              Service.(link search_sign ())
              [H.txt "Signs"]
          ; Service.Link.a ~cl:"home-search-inscription"
              Service.(link search_inscription ())
              [H.txt "Documents"]
          ; Service.Link.a ~cl:"home-search-sequence"
              Service.(link search_sequence ())
              [H.txt "Sequences"]
          ; Service.Link.a ~cl:"home-sign-list"
              Service.(link Service.signs ())
              [H.txt "Sign list"]
          ; Service.Link.a ~cl:"home-document-list"
              Service.(link Service.browse ())
              [H.txt "Browse corpus"]
          ; Service.Link.a ~cl:"home-map"
              Service.(link Service.maps ())
              [H.txt "Map"] ] ]
end

module PSearch = struct
  let results (Search.Search search) =
    let tit =
      [ H.txt "%s search" (String.capitalize_ascii @@ Ty.show search.typ)
      ; H.txt " [ "
      ; Service.Link.text (Service.(link search) (Search search)) "edit"
      ; H.txt " ]" ]
    in
    let form =
      Form.(
        run (input_search search.typ) (Some search) (fun s ->
            Service.(run search_results (Search s)) ))
    in
    fill_page tit
      ( form.html @ [H.br ()]
      @ Display.Grouping.show Socket.db search.typ search.settings
          (Search.exec Socket.db search) )

  let form (Search.Search search) =
    let form =
      Form.(
        run (input_search search.typ) (Some search) (fun s ->
            Service.(run search_results (Search s)) ))
    in
    let tit =
      [H.txt "%s Search" (String.capitalize_ascii (Ty.show search.typ))]
    in
    fill_page tit form.html
end

module View = struct
  let site name = PSearch.results (Search.document ~location:[name] ())

  let period name = PSearch.results (Search.document ~period:[name] ())

  let kind name =
    PSearch.(results (LinearA.Search.document ~types:[name] ()))

  let document (doc_name, word_view, selected) =
    let doc =
      try Database.find_document Socket.db doc_name
      with _ -> failwith @@ Printf.sprintf "Document %s not found" doc_name
    in
    let size =
      H.when_def (Document.size doc) (fun (a, b, c) ->
          H.txt "(%g cm x %g cm x %g cm)" a b c )
    in
    let () = Display.reset_sign_info () in
    let title = [H.txt "Document %s" doc_name] in
    let view = ref (if word_view then `Word else `Sign) in
    let selected = ref selected in
    let signs =
      let n = Array.length @@ Document.signs doc in
      H.txt "%d sign%s " n (if n > 0 then "s" else "")
    in
    let words =
      let n = Array.length (Document.sequences doc) in
      H.txt "%d word%s " n (if n > 0 then "s" else "")
    in
    let rec set_word_view () =
      view := `Word ;
      selected := None ;
      redraw ()
    and set_sign_view () =
      view := `Sign ;
      selected := None ;
      redraw ()
    and metadata () =
      let sign_view =
        if !view = `Sign then H.empty
        else H.action set_sign_view [H.txt "[ sign view ]"]
      in
      let word_view =
        if !view = `Word then H.empty
        else H.action set_word_view [H.txt "[ word view ]"]
      in
      H.div ~cl:["document-metadata"]
        ( [ Service.(
              Link.text (link kind (Document.kind doc)) (Document.kind doc))
          ; H.txt " found at "
          ; Service.(
              Link.text
                (link location (Document.location doc))
                (Document.location doc))
          ; H.txt " "
          ; size ]
        @ ( match Document.period doc with
          | Some s ->
              H.
                [ br ()
                ; txt "Period: "
                ; Service.Link.(text (L (Service.period, s)) s) ]
          | _ -> [] )
        @ ( match Document.url doc with
          | Some s -> H.[br (); a ~a:[a_href s] [H.txt "Link to corpus"]]
          | _ -> [] )
        @ [H.br (); signs; sign_view; H.txt " / "; words; word_view] )
    and redraw () =
      let popup_style = if !selected = None then `Floating else `Fixed in
      let doc_html =
        ( if !view = `Sign then
          Display.Document.svg_with_signs ~popup_style ?selected:!selected
        else Display.Document.svg_with_words ~popup_style ?selected:!selected
        )
          doc
      in
      fill_page title
        [ metadata ()
        ; H.div
            ~a:[H.a_style "text-align:center"]
            [H.div ~a:[H.a_id "document-view"] doc_html]
        ; H.guard (Document.comment doc <> None)
          @@ H.div ~cl:["document-notes"]
               [ H.txt "Comment on the document:"
               ; Widget.show_note
                   (fun s k ->
                     Socket.ask
                       (Protocol.EditDocumentComment (Document.name doc, s))
                       k )
                   (Document.comment doc) ] ]
    in
    redraw ()

  let sign si =
    PSearch.results LinearA.Search.(sign ~sign:(pattern_of_meta si) ())

  let sign_list () =
    let show_instance instance =
      let open Script in
      let si = instance.sign in
      let signnumber =
        H.concat' (fun () -> [H.txt "/"])
        @@ List.filter_map
             (fun x -> x)
             [ Some (Display.Show.(Instance.show Sign.show_formal) instance)
             ; Option.map Display.Show.PhoneticValue.show si.phonetic
             ; Option.map (fun x -> [H.txt_ x]) si.latin ]
      in
      if instance.variant = None && si.rep = None then H.empty
      else
        H.div ~cl:["flex-table-item"]
          [ H.span ~a:[H.a_class ["signnumber"]] signnumber
          ; Service.(Link.a (Link.instance instance))
              ( match
                  Display.Show.Instance.image ~cl:"sign-image-big" instance
                with
              | [] ->
                  [ H.span ~cl:["sign-image-big"]
                      (Display.Show.Sign.show_formal si) ]
              | l -> l )
          ; ( match Display.Show.decomposition ~prefix:"= " si with
            | [] -> H.empty
            | l -> H.span l ) ]
    in
    let instances = Script.Instance.list () in
    let simple, composite =
      List.partition
        (fun {Script.sign; _} -> sign.Script.composition = None)
        instances
    in
    let normal, fraction =
      List.partition
        (fun {Script.sign; _} ->
          match Script.Sign.default_role sign with
          | Role.Fraction, _ -> false
          | _ -> true )
        simple
    in
    let table_of c l =
      H.div ~cl:["flex-table"; c] (List.map show_instance l)
    in
    fill_page [H.txt "Sign list"]
      [ H.h2 [H.txt "Simple signs"]
      ; table_of "small" normal
      ; H.h2 [H.txt "Composite signs"]
      ; table_of "big" composite
      ; H.h2 [H.txt "Fraction signs"]
      ; table_of "small" fraction ]
end

module Map = struct
  let make_map (image_size, png, rects) =
    let make_link (rect, link) =
      S.a ~a:[Service.Link.s_href link] [S.rect ["map-link"] rect]
    in
    H.div ~cl:["map"]
      [ H.make_svg png
          ~size:((100., Some `Percent), (100., Some `Percent))
          ~image_size
          (List.map make_link rects) ]

  let crete =
    ( (1731, 755)
    , "maps/crete.png"
    , [ ( Rect.{x = 255; y = 192; w = 77; h = 55}
        , Service.(link location "Khania") )
      ; ( Rect.{x = 867 - 80; y = 532 - 55; w = 80; h = 55}
        , Service.(link location "Phaistos") )
      ; ( Rect.{x = 967; y = 386; w = 100; h = 55}
        , Service.(link location "Arkhanes") )
      ; ( Rect.{x = 948; y = 304; w = 100; h = 86}
        , Service.(link location "Knossos") )
      ; ( Rect.{x = 1159; y = 322; w = 67; h = 62}
        , Service.(link location "Mallia") )
      ; ( Rect.{x = 844; y = 337; w = 119; h = 39}
        , Service.(link location "Tylissos") )
      ; ( Rect.{x = 1643; y = 458; w = 80; h = 57}
        , Service.(link location "Zakros") )
      ; ( Rect.{x = 800 - 150; y = 507; w = 150; h = 100}
        , Service.(link location "Haghia Triada") ) ] )

  let maps () = fill_page [H.txt "Maps of Linear A sites"] [make_map crete]
end

let documents () = PSearch.results (LinearA.Search.document ())

let login password =
  Home.display () ;
  Socket.login password ;
  H.Notification.success "Logged in!"
